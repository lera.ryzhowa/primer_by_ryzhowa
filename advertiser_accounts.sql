--Добавляем логин рекламодателя
SELECT number,
       created_date,
       finally_amount,
       finally_currency,
       status,
       date_pay_before,
       pay_voucher_date,
       finally_amount_USD,
       payed_amount,
       affiliate_program,
       login_advertiser,
       legal_entity_name,
       team_of_programs,
       manager_of_action
FROM (--Добавляем команду и менеджера
      SELECT number,
             affiliate_program,
             advcampaign_id,
             multiIf(status = 'payed', 'paid',
                     status = 'partially_payed', 'partially_paid',
                     status = 'cancellation', 'cancellation',
                     status = 'issued', 'issued',
                     'canceled') AS status,
             created_date,
             date_pay_before,
             pay_voucher_date,
             finally_amount,
             payed_amount,
             finally_currency,
             finally_amount_USD,
             legal_entity_name,
             team_of_programs,
             manager_of_action
      FROM (--Добавляем срок оплаты, где не проставлено
               SELECT number,
                      advcampaign_id,
                      created_date,
                      affiliate_program,
                      finally_amount,
                      finally_currency,
                      finally_amount_USD,
                      status,
                      payed_amount,
                      legal_entity_name,
                      date_pay_before,
                      if(pay_voucher_date != '1970-01-01', pay_voucher_date, date_deposit) AS pay_voucher_date --Дата пополнения счета, если дата оплаты не подтянулась
               FROM (--Добавляем срок оплаты по договору и дату оплаты, если есть
                        SELECT number,
                               advcampaign_id,
                               created_date,
                               affiliate_program,
                               finally_amount,
                               finally_currency,
                               finally_amount_USD,
                               status,
                               payed_amount,
                               legal_entity_name,
                               if(date_pay_before != '1970-01-01', date_pay_before, NULL) AS date_pay_before, --Пустая ячейка, если дата не подтянулась
                               pay_voucher_date
                        FROM (--Выбираем нужные столбцы из advcampaign_bill
                                 SELECT number,
                                        advcampaign_id,
                                        toDate(created_datetime)                                             AS created_date,
                                        dictGetString('default.campaigns', 'name', toUInt64(advcampaign_id)) AS affiliate_program,
                                        argMax(finally_amount, snapshot_datetime)                            AS finally_amount,
                                        argMax(finally_currency, snapshot_datetime)                          AS finally_currency,
                                        --Вычисляем сумму транзакции в долларах
                                        if(finally_currency = 'USD', toFloat64(finally_amount),
                                           toFloat64(dictGet('default.currency', 'rate',
                                                             tuple(finally_currency, 'USD', created_date))) *
                                           toFloat64(finally_amount))                                        AS finally_amount_USD, --Переводим сумму счета в доллары по курсу на день выставления счета
                                        argMax(status, snapshot_datetime)                                    AS status,
                                        argMax(payed_amount, snapshot_datetime)                              AS payed_amount,
                                        argMax(legal_entity_name, snapshot_datetime)                         AS legal_entity_name
                                 FROM advcampaign_bill
                                 WHERE created_datetime BETWEEN $from AND $to --Фильтр на дату выставления счета
                                       $conditionalTest(AND number IN ('$bill'), $bill) --Фильтр на номер счета
                                 GROUP BY number, advcampaign_id, created_date
                                 HAVING 1 $conditionalTest(AND status IN ($status), $status) --Фильтр на статус
                                        $conditionalTest(AND finally_currency IN ($currency), $currency) --Фильтр на валюту
                                        $conditionalTest(AND toUInt64(legal_entity_name) IN ($entity), $entity) --Фильтр на юр.лицо
                                        $conditionalTest(AND affiliate_program IN ($program), $program) -- Фильтр на партнерскую программу
                                 )

                                 ANY LEFT JOIN
                                 (--Добавляем срок оплаты по договору и дату оплаты, если есть
                                    SELECT argMax(date_pay_before, snapshot_datetime)  AS date_pay_before,
                                           argMax(pay_voucher_date, snapshot_datetime) AS pay_voucher_date,
                                           account_number                              AS number,
                                           argMax(status_id, snapshot_datetime)        AS status_bitrix
                                    FROM invoices_for_advertisers
                                    GROUP BY number
                           )
                           USING number
                        )
                        ANY LEFT JOIN
                        (--Добавляем срок оплаты, где не проставлено
                           SELECT bill_number                                 AS number,
                                  toDate(argMax(datetime, snapshot_datetime)) AS date_deposit
                           FROM deposit
                           GROUP BY number
                           HAVING argMax(status, snapshot_datetime) = 1 --Берем обработанные пополнения баланса
                        )
                       USING number
               WHERE pay_voucher_date BETWEEN toDate('$payment_date_from') AND toDate('$payment_date_to') --Фильтр на дату оплаты
               )
               ANY LEFT JOIN
               (--Добавляем команду и менеджера
                  SELECT multiIf(team_of_action = 'OD-Cashback', 'OD-Cashback',
                                 team_of_program IN ('OD-Affiliate Network-Russia-Aliexpress',
                                                     'OD-Affiliate Network-Russia-Ecommerce',
                                                     'OD-Affiliate Network-Russia-Finance',
                                                     'OD-Affiliate Network-Russia-Games',
                                                     'OD-Affiliate Network-Russia-MobAppl',
                                                     'OD-Affiliate Network-Russia-MobMarket',
                                                     'OD-Affiliate Network-Russia-OnlineServices',
                                                     'OD-Affiliate Network-Europe-DACH',
                                                     'OD-Affiliate Network-Europe-France',
                                                     'OD-Affiliate Network-Europe-Italy',
                                                     'OD-Affiliate Network-Europe-Netherlands',
                                                     'OD-Affiliate Network-Europe-Spain',
                                                     'OD-Affiliate Network-Europe-UK',
                                                     'OD-Affiliate Network-Europe-Poland',
                                                     'OD-Affiliate Network-Europe-RestEurope',
                                                     'OD-Affiliate Network-LATAM-Brazil',
                                                     'OD-Affiliate Network-LATAM-RestLATAM',
                                                     'OD-Affiliate Network-MENA',
                                                     'OD-Affiliate Network-NothAmerica',
                                                     'OD-Affiliate Network-APAC-India-Ecommerce',
                                                     'OD-Affiliate Network-APAC-India-Finance',
                                                     'OD-Affiliate Network-APAC-India-OnlineServices',
                                                     'OD-Affiliate Network-APAC-India-RestSegments',
                                                     'OD-Affiliate Network-APAC-OutsideChina',
                                                     'OD-Affiliate Network-APAC-RestAPAC',
                                                     'OD-Affiliate Network-CIS-Belarus',
                                                     'OD-Affiliate Network-CIS-Kazakhstan',
                                                     'OD-Affiliate Network-CIS-RestCIS',
                                                     'OD-Affiliate Network-CIS-Ukraine-Ecommerce',
                                                     'OD-Affiliate Network-CIS-Ukraine-Finance',
                                                     'OD-Affiliate Network-CIS-Ukraine-OnlineServices',
                                                     'OD-Affiliate Network-CIS-Ukraine-RestSegments',
                                                     'OD-Affiliate Network-CIS-Ukraine-SME',
                                                     'OD-Cashback'
                                ), team_of_program,
                                'Unknown_team') AS team_of_programs,
                        program_id              AS advcampaign_id,
                        manager_of_action
               FROM report.aggregated_revenue
               WHERE report_datetime in (SELECT max(report_datetime)
                                         FROM report.aggregated_revenue)
               )
      USING advcampaign_id
      WHERE 1 $conditionalTest(AND team_of_programs IN ($team_of_programs), $team_of_programs) --Фильтр на название команды
         )
ANY LEFT JOIN
         (--Присоединяем логин рекламодателя
            SELECT argMax(user__username, snapshot_datetime) AS login_advertiser,
                   toUInt32(object_id)                       AS advcampaign_id
            FROM affiliate_program
            WHERE snapshot_datetime > '2020-08-01 00:00:00'
            GROUP BY advcampaign_id
)
USING advcampaign_id
HAVING 1 $conditionalTest(AND login_advertiser IN ($advertiser), $advertiser) --Фильтр на логин рекламодателя
ORDER BY created_date