SELECT months                               AS month,
       toUInt32(program_id)                 AS program_id,
       toFloat64(
               if(dateDiff('month', toStartOfMonth(program_start), toStartOfMonth(months)) > 6 AND aprvd_dclnd_3 > 1000,
                  PRCTG_1,
                  child_category_prctg_v1)) AS percent
FROM (--Присоеденяем средний процент по категории для старый и не активных программ, считаем процент для всех программ
         SELECT program_id,
                addMonths(month_row[num], 0)                              AS months,
                if(num <= 3, 0, avg_sum_app_dec_3[num - 3])               AS aprvd_dclnd_3,
                if(num <= 3 OR avg_sum_app_dec_3[num - 3] = 0, 0,
                   (avg_sum_app_3[num - 3]) / avg_sum_app_dec_3[num - 3]) AS PRCTG_1,
                child_category,
                program_start
         FROM (--считаем скользящие суммы за 3 месяца по всем программам и присоедениям счетчик и категории
                  SELECT program_id,
                         groupArray(month)                              AS month_row,
                         groupArrayMovingSum(3)(gross_revenue_approved) AS avg_sum_app_3,
                         groupArrayMovingSum(3)(gross_revenue_app_dec)  AS avg_sum_app_dec_3
                  FROM (
                        SELECT program_id,
                               month,
                               gross_revenue_approved,
                               gross_revenue_app_dec
                        FROM (--Присоеденяем суммы по программам
                                 SELECT program_id,
                                        month
                                 FROM (--Выбираем все существующие месяца
                                          SELECT DISTINCT month AS month
                                          FROM report.aggregated_revenue
                                          ) AS T1
                                          CROSS JOIN
                                      (--Декартово произведение со всеми существующими программами
                                          SELECT DISTINCT program_id
                                          FROM report.aggregated_revenue
                                          )
                                 ) AS A1
                                 ANY
                                 LEFT JOIN
                             (--Суммируем выручку по программам
                                 SELECT program_id,
                                        month,
                                        toFloat64(sumIf(final_payment_usd + final_payment_to_system_usd,
                                                        last_status = 'approved'))                AS gross_revenue_approved,
                                        toFloat64(sumIf(final_payment_usd + final_payment_to_system_usd,
                                                        last_status IN ('approved', 'declined'))) AS gross_revenue_app_dec
                                 FROM report.aggregated_revenue
                                 WHERE report_datetime IN
                                       (SELECT max(report_datetime) FROM report.aggregated_revenue)
                                   AND program_id NOT IN (14893, 17789, 21659)
                                   AND webmaster_id != 598980
                                 GROUP BY program_id, month
                                 ORDER BY month ASC
                                 ) AS A2
                             ON A1.month = A2.month AND A1.program_id = A2.program_id
                        ORDER BY month ASC
                           )
                  GROUP BY program_id
                  )
                  ARRAY JOIN
              arrayEnumerate(month_row) AS num
                  LEFT JOIN
              (--джойним с категориями
                  SELECT program_id,
                         toUInt32(arrayElement(latest_child_category, 1)) AS child_category,
                         program_start
                  FROM (--берем сочетания программы и категории
                        SELECT id                                                    AS program_id,
                               argMax(child_categories_internal, snapshot_timestamp) AS latest_child_category,
                               argMax(activation_date, snapshot_timestamp)           AS program_start
                        FROM default.affiliate_program
                        GROUP BY id
                           )
                  )
              USING program_id
         ) AS T1
         LEFT JOIN
     (--Присоединяем средний процент по категории
         SELECT child_category,
                months,
                avg(PRCTG_1) AS child_category_prctg_v1
         FROM (--Считаем процент подтверждения только у новых и активных программ
               SELECT child_category,
                      addMonths(month_row[num], 0)                              AS months,
                      program_id,
                      avg_sum_app_3[num - 3],
                      if(num <= 3 OR avg_sum_app_3[num - 3] = 0 OR avg_sum_app_dec_3[num - 3] = 0, 0,
                         (avg_sum_app_3[num - 3]) / avg_sum_app_dec_3[num - 3]) AS PRCTG_1
               FROM (--считаем скользящие суммы за 3 месяца по всем программам и присоедениям счетчик и категории
                        SELECT program_id,
                               groupArray(month)                              AS month_row,
                               groupArrayMovingSum(3)(gross_revenue_approved) AS avg_sum_app_3,
                               groupArrayMovingSum(3)(gross_revenue_app_dec)  AS avg_sum_app_dec_3
                        FROM (SELECT program_id,
                                     month,
                                     gross_revenue_approved,
                                     gross_revenue_app_dec
                              FROM (--Присоеденяем суммы по программам
                                       SELECT program_id,
                                              month
                                       FROM (--Выбираем все существующие месяца
                                                SELECT DISTINCT month AS month
                                                FROM report.aggregated_revenue
                                                ) AS T1
                                                CROSS JOIN
                                            (--Декартово произведение со всеми существующими программами
                                                SELECT DISTINCT program_id
                                                FROM report.aggregated_revenue
                                                )
                                       ) AS A1
                                       ANY
                                       LEFT JOIN
                                   (--Суммируем выручку по программам
                                       SELECT program_id,
                                              month,
                                              toFloat64(sumIf(final_payment_usd + final_payment_to_system_usd,
                                                              last_status = 'approved'))                AS gross_revenue_approved,
                                              toFloat64(sumIf(final_payment_usd + final_payment_to_system_usd,
                                                              last_status IN ('approved', 'declined'))) AS gross_revenue_app_dec
                                       FROM report.aggregated_revenue
                                       WHERE report_datetime IN
                                             (SELECT max(report_datetime) FROM report.aggregated_revenue)
                                         AND program_id NOT IN (14893, 17789, 21659)
                                         AND webmaster_id != 598980
                                       GROUP BY program_id, month
                                       ORDER BY month ASC
                                       ) AS A2
                                   ON A1.month = A2.month AND A1.program_id = A2.program_id
                              ORDER BY month ASC
                                 )
                        GROUP BY program_id
                        )
                        ARRAY JOIN
                    arrayEnumerate(month_row) AS num
                        LEFT JOIN
                    (--джойним с категориями
                        SELECT program_id,
                               program_start,
                               toUInt32(arrayElement(latest_child_category, 1)) AS child_category
                        FROM (--берем сочетания программы и категории
                              SELECT id                                                    AS program_id,
                                     argMax(child_categories_internal, snapshot_timestamp) AS latest_child_category,
                                     argMax(activation_date, snapshot_timestamp)           AS program_start
                              FROM default.affiliate_program
                              GROUP BY id
                                 )
                        )
                    USING program_id
               WHERE dateDiff('month', toStartOfMonth(program_start), toStartOfMonth(months)) > 6
                 AND num > 3
                 AND avg_sum_app_dec_3[num - 3] > 1000
                  )
         GROUP BY child_category, months
         ) AS T2
     ON T1.months = T2.months AND T1.child_category = T2.child_category
ORDER BY program_id, months DESC