--добавляем логин и категорию партнерской программы
SELECT offer_id,
       offer_name,
       offer_login,
       offer_category,
       web_id,
       if(web_login = '', 'deleted', web_login) AS web_login,
       country_of_action,
       apr_web_commission_orig,
       apr_system_commission_orig,
       apr_total_commission_orig,
       percent,
       forecast_web_commission_orig,
       forecast_system_commission_orig,
       forecast_total_commission_orig,
       apr_forecast_web_commission_orig,
       apr_forecast_system_commission_orig,
       apr_forecast_total_commission_orig,
       apr_forecast_web_commission_usd,
       apr_forecast_system_commission_usd,
       apr_forecast_total_commission_usd,
       ali_antispam_correction_web_gross,
       ali_antispam_correction_to_system_gross,
       ali_antispam_correction_total_gross,
       ali_bonus_correction_web_gross,
       ali_bonus_correction_to_system_gross,
       ali_bonus_correction_total_gross,
       ali_deeplink_correction_web_gross,
       ali_deeplink_correction_to_system_gross,
       ali_deeplink_correction_total_gross,
       final_commission_web_usd_apr_forecast_ali_gross,
       final_commission_to_system_usd_apr_forecast_ali_gross,
       final_commission_total_usd_apr_forecast_ali_gross,
       team_main_account,
       team_sub_account,
       gross_revenue_main_account_usd,
       gross_revenue_sub_account_usd,
       gross_revenue_total_usd,
       ali_antispam_correction_to_system_net,
       ali_bonus_correction_to_system_net,
       ali_deeplink_correction_to_system_net,
       net_revenue_main_account_usd,
       net_revenue_sub_account_usd,
       net_revenue_tax_usd,
       net_revenue_total_usd
FROM (SELECT offer_id,
        dictGetString('default.campaigns', 'name', toUInt64(offer_id)) AS offer_name,
        toString(web_id) AS web_id,
        dictGet('default.hq_webmasters', 'webmaster_username', tuple(toUInt32(web_id), toDate(date_sub(DAY, 1, toDate('$report_day'))))) AS web_login,
        country_of_action,
        apr_web_commission_orig,
        apr_system_commission_orig,
        apr_total_commission_orig,
        percent,
        forecast_web_commission_orig,
        forecast_system_commission_orig,
        forecast_total_commission_orig,
        apr_forecast_web_commission_orig,
        apr_forecast_system_commission_orig,
        apr_forecast_total_commission_orig,
        source_currency,
        apr_forecast_web_commission_usd,
        apr_forecast_system_commission_usd,
        apr_forecast_total_commission_usd,
        multiIf(
                offer_id = 6115, if(apr_web_ali_usd = 0, 0,
                                    (web_payments / apr_web_ali_usd) * 0.8 * antispam),
                offer_id = 22586, if(apr_web_ali_usd = 0, 0,
                                     (web_payments / apr_web_ali_usd) * 0.88 * antispam),
                0) AS ali_antispam_correction_web_gross,
        multiIf(
                offer_id = 6115, if(apr_to_system_ali = 0, 0,
                                    apr_system_commission_usd /
                                    apr_to_system_ali) * 0.2 * antispam,
                offer_id = 22586, if(apr_to_system_ali = 0, 0,
                                     apr_system_commission_usd /
                                     apr_to_system_ali) * 0.12 * antispam,
                0) AS ali_antispam_correction_to_system_gross,
        ali_antispam_correction_web_gross +
        ali_antispam_correction_to_system_gross AS ali_antispam_correction_total_gross,
        0 AS ali_bonus_correction_web_gross,
        if(
                offer_id IN (6115, 22586), if(apr_to_system_ali = 0, 0,
                                              apr_system_commission_usd /
                                              apr_to_system_ali) * bonus,
                0) AS ali_bonus_correction_to_system_gross,
        ali_bonus_correction_to_system_gross AS ali_bonus_correction_total_gross,
        0 AS ali_deeplink_correction_web_gross,
        if(
                offer_id = 22586, if(apr_to_system_ali = 0, 0,
                                     apr_system_commission_usd / apr_to_system_ali) * deeplink,
                0) AS ali_deeplink_correction_to_system_gross,
        ali_deeplink_correction_to_system_gross AS ali_deeplink_correction_total_gross,
        apr_forecast_web_commission_usd + ali_antispam_correction_web_gross AS final_commission_web_usd_apr_forecast_ali_gross,
        apr_forecast_system_commission_usd + ali_antispam_correction_to_system_gross +
        ali_bonus_correction_to_system_gross +
        ali_deeplink_correction_to_system_gross AS final_commission_to_system_usd_apr_forecast_ali_gross,
        final_commission_web_usd_apr_forecast_ali_gross +
        final_commission_to_system_usd_apr_forecast_ali_gross AS final_commission_total_usd_apr_forecast_ali_gross,
        multiIf(team_of_action = 'OD-Cashback', 'OD-Cashback',
                dictGetString('default.hq_program_managers', 'frc_colour',
                              tuple(toUInt32(offer_id), toDate(toDate(date_sub(DAY, 1, toDate('$report_day')))), 'Main')) = 'green',
                team_of_program,
                'Unknown_team') AS team_main_account,
        if(dictGetString('default.hq_program_managers', 'frc_colour',
                     tuple(toUInt32(offer_id), toDate(toDate(date_sub(DAY, 1, toDate('$report_day')))), 'Main')) = 'green', team_of_action, 'Unknown_team') AS team_sub_account,
        0 AS gross_revenue_main_account_usd,
        gross_revenue_total_usd AS gross_revenue_sub_account_usd,
        final_commission_total_usd_apr_forecast_ali_gross AS gross_revenue_total_usd,
                multiIf(
                        offer_id = 6115, if(apr_to_system_ali + web_month_ali = 0, 0,
                                            (apr_system_commission_usd + web_revenue_approved) / (
                                                apr_to_system_ali + web_month_ali)) * 0.2 * antispam,
                        offer_id = 22586, if(apr_to_system_ali + web_month_ali = 0, 0,
                                             (apr_system_commission_usd + web_revenue_approved) / (
                                                 apr_to_system_ali + web_month_ali)) * 0.12 * antispam,
                        0)                                                               AS ali_antispam_correction_to_system_net,
                if(
                        offer_id IN (6115, 22586), if(apr_to_system_ali + web_month_ali = 0, 0,
                                                      (apr_system_commission_usd + web_revenue_approved) / (
                                                          apr_to_system_ali + web_month_ali)) * bonus,
                        0)                                                               AS ali_bonus_correction_to_system_net,
                if(
                        offer_id = 22586, if(apr_to_system_ali + web_month_ali = 0, 0,
                                             (apr_system_commission_usd + web_revenue_approved) / (
                                                 apr_to_system_ali + web_month_ali)) * deeplink,
                        0)                                                               AS ali_deeplink_correction_to_system_net,
                if(team_main_account = team_sub_account, net_revenue_total_usd, 0)       AS net_revenue_main_account_usd,
                if(team_main_account = team_sub_account, 0, 0.6 * net_revenue_total_usd) AS net_revenue_sub_account_usd,
                if(team_main_account = team_sub_account, 0, 0.4 * net_revenue_total_usd) AS net_revenue_tax_usd,
                apr_forecast_system_commission_usd_net + ali_antispam_correction_to_system_net
                + ali_bonus_correction_to_system_net + ali_deeplink_correction_to_system_net  AS net_revenue_total_usd
            FROM (
                  SELECT team_of_action,
                         team_of_program,
                         webmaster_id                                                         AS web_id,
                         program_id                                                           AS offer_id,
                         country_of_action,
                         sum(web_payment_orig) + sum(subscription_fee_marathons_orig)         AS apr_web_commission_orig,
                         sum(apr_to_system_orig)                                              AS apr_system_commission_orig,
                         apr_web_commission_orig + apr_system_commission_orig                 AS apr_total_commission_orig,
                         percent,
                         sum(forecast_web_orig)                                               AS forecast_web_commission_orig,
                         sum(forecast_system_orig)                                            AS forecast_system_commission_orig,
                         forecast_web_commission_orig + forecast_system_commission_orig       AS forecast_total_commission_orig,
                         forecast_web_commission_orig + apr_web_commission_orig               AS apr_forecast_web_commission_orig,
                         forecast_system_commission_orig + apr_system_commission_orig         AS apr_forecast_system_commission_orig,
                         forecast_total_commission_orig + apr_total_commission_orig           AS apr_forecast_total_commission_orig,
                         source_currency,
                         sum(subscription_fee_marathons) + sum(web_payment)                   AS apr_web_commission_usd,
                         sum(web_payment) AS web_payments,
                         sum(forecast_web_usd) + apr_web_commission_usd                       AS apr_forecast_web_commission_usd,
                         sum(apr_system_usd)                                                  AS apr_system_commission_usd,
                         sum(forecast_system_usd) + apr_system_commission_usd                 AS apr_forecast_system_commission_usd,
                         apr_forecast_web_commission_usd + apr_forecast_system_commission_usd AS apr_forecast_total_commission_usd,
                         multiIf(program_id = 6115, toFloat64(dictGet('default.admitad_indicators', 'antispam_amount',
                                                                      tuple(toDate(month), toUInt32(program_id)))),
                                 program_id = 22586, toFloat64(dictGet('default.admitad_indicators', 'antispam_amount',
                                                                       tuple(toDate(month), toUInt32(program_id)))),
                                 0)                                                           AS antispam,
                         multiIf(program_id = 6115, toFloat64(dictGet('default.admitad_indicators', 'bonus_amount',
                                                                      tuple(toDate(month), toUInt32(program_id)))),
                                 program_id = 22586, toFloat64(dictGet('default.admitad_indicators', 'bonus_amount',
                                                                       tuple(toDate(month), toUInt32(program_id)))),
                                 0)                                                           AS bonus,
                         if(program_id = 22586, toFloat64(dictGet('default.admitad_indicators', 'deeplink_amount',
                                                                  tuple(toDate(month), toUInt32(program_id)))),
                            0)                                                                AS deeplink,
                        sum(web_revenue_approved) AS web_revenue_approved,
                        sum(subscription_fee_marathons) + apr_system_commission_usd + sum(web_revenue_forecast) + sum(forecast_system_usd) +
                        web_revenue_approved     AS apr_forecast_system_commission_usd_net

                  FROM (
                   SELECT toFloat64(final_payment_orig_currency)                                          AS final_payment_orig_currency,
                          toFloat64(final_payment_to_system_orig_currency)                                AS final_payment_to_system_orig_currency,
                          team_of_action,
                          team_of_program,
                          month,
                          webmaster_id,
                          program_id,
                          country_of_action,
                          if(last_status = 'approved' AND webmaster_id NOT IN (393445,
                                                                               1028843,
                                                                               683873,
                                                                               275729
                              ), final_payment_orig_currency,
                             0)                                                                           AS web_payment_orig,
                          if(last_status = 'approved' AND webmaster_id IN (393445,
                                                                           1028843,
                                                                           683873,
                                                                           275729
                              ), final_payment_orig_currency,
                             0)                                                                           AS subscription_fee_marathons_orig,
                          if(last_status = 1, toFloat64(final_payment_to_system_orig_currency),
                             0)                                                                           AS apr_to_system_orig,
                          toFloat64(dictGet('default.approval_percents', 'percent',
                                            tuple(toDate(toDate(date_sub(DAY, 1, toDate('$report_day')))), month, toUInt32(program_id)))) AS percent,
                          multiIf(final_payment_orig_currency + final_payment_to_system_orig_currency >
                                  toUInt32(60000) AND
                                  last_status = 'pending'
                                      AND webmaster_id NOT IN (393445, -- adm_green
                                                               1028843, -- ad_ua_green
                                                               683873, -- 00advertiser00
                                                               275729, -- admitad_promo
                                                               90289, --gnatovsky
                                                               1355412, --webhostadvice
                                                               1118196, --ankurkhanna
                                                               607243, --megabonuscom
                                                               701906 --aliexpressradar
                                  ), 0,
                                  final_payment_orig_currency + final_payment_to_system_orig_currency <
                                  toUInt32(60000) AND
                                  last_status = 'pending'
                                      AND webmaster_id NOT IN (393445, -- adm_green
                                                               1028843, -- ad_ua_green
                                                               683873, -- 00advertiser00
                                                               275729, -- admitad_promo
                                                               90289, --gnatovsky
                                                               1355412, --webhostadvice
                                                               1118196, --ankurkhanna
                                                               607243, --megabonuscom
                                                               701906 --aliexpressradar
                                      ), final_payment_orig_currency * percent,
                                  last_status = 'pending' AND webmaster_id IN (90289, --gnatovsky
                                                                               1355412, --webhostadvice
                                                                               1118196, --ankurkhanna
                                                                               607243, --megabonuscom
                                                                               701906 --aliexpressradar
                                      ), final_payment_orig_currency * percent,
                                  0
                              )                                                                           AS forecast_web_orig,
                          multiIf(final_payment_orig_currency + final_payment_to_system_orig_currency >
                                  toUInt32(60000) AND
                                  last_status = 'pending'
                                      AND webmaster_id NOT IN (393445, -- adm_green
                                                               1028843, -- ad_ua_green
                                                               683873, -- 00advertiser00
                                                               275729, -- admitad_promo
                                                               90289, --gnatovsky
                                                               1355412, --webhostadvice
                                                               1118196, --ankurkhanna
                                                               607243, --megabonuscom
                                                               701906 --aliexpressradar
                                  ), 0,
                                  final_payment_orig_currency + final_payment_to_system_orig_currency <
                                  toUInt32(60000) AND
                                  last_status = 'pending'
                                      AND webmaster_id NOT IN (393445, -- adm_green
                                                               1028843, -- ad_ua_green
                                                               683873, -- 00advertiser00
                                                               275729, -- admitad_promo
                                                               90289, --gnatovsky
                                                               1355412, --webhostadvice
                                                               1118196, --ankurkhanna
                                                               607243, --megabonuscom
                                                               701906 --aliexpressradar
                                      ), final_payment_to_system_orig_currency * percent,
                                  last_status = 'pending' AND webmaster_id IN (90289, --gnatovsky
                                                                               1355412, --webhostadvice
                                                                               1118196, --ankurkhanna
                                                                               607243, --megabonuscom
                                                                               701906 --aliexpressradar
                                      ), final_payment_to_system_orig_currency * percent,
                                  0
                              )                                                                           AS forecast_system_orig,
                          source_currency,
                          toFloat64(final_payment_usd)                                                    AS final_payment_usd,
                          toFloat64(final_payment_to_system_usd)                                          AS final_payment_to_system_usd,
                          if(last_status = 'approved' AND webmaster_id NOT IN (393445,
                                                                               1028843,
                                                                               683873,
                                                                               275729
                              ), final_payment_usd,
                             0)                                                                           AS web_payment,
                          if(last_status = 'approved' AND webmaster_id IN (393445,
                                                                           1028843,
                                                                           683873,
                                                                           275729
                              ), final_payment_usd,
                             0)                                                                           AS subscription_fee_marathons,
                          if(last_status = 1, toFloat64(final_payment_to_system_usd),
                             0)                                                                           AS apr_system_usd,
                          multiIf(final_payment_usd + final_payment_to_system_usd > toUInt32(60000) AND
                                  last_status = 'pending'
                                      AND webmaster_id NOT IN (393445, -- adm_green
                                                               1028843, -- ad_ua_green
                                                               683873, -- 00advertiser00
                                                               275729, -- admitad_promo
                                                               90289, --gnatovsky
                                                               1355412, --webhostadvice
                                                               1118196, --ankurkhanna
                                                               607243, --megabonuscom
                                                               701906 --aliexpressradar
                                  ), 0,
                                  final_payment_usd + final_payment_to_system_usd < toUInt32(60000) AND
                                  last_status = 'pending'
                                      AND webmaster_id NOT IN (393445, -- adm_green
                                                               1028843, -- ad_ua_green
                                                               683873, -- 00advertiser00
                                                               275729, -- admitad_promo
                                                               90289, --gnatovsky
                                                               1355412, --webhostadvice
                                                               1118196, --ankurkhanna
                                                               607243, --megabonuscom
                                                               701906 --aliexpressradar
                                      ), final_payment_usd * percent,
                                  last_status = 'pending' AND webmaster_id IN (90289, --gnatovsky
                                                                               1355412, --webhostadvice
                                                                               1118196, --ankurkhanna
                                                                               607243, --megabonuscom
                                                                               701906 --aliexpressradar
                                      ), final_payment_usd * percent,
                                  0
                              )                                                                           AS forecast_web_usd,
                          multiIf(final_payment_usd + final_payment_to_system_usd > toUInt32(60000) AND
                                  last_status = 'pending'
                                      AND webmaster_id NOT IN (393445, -- adm_green
                                                               1028843, -- ad_ua_green
                                                               683873, -- 00advertiser00
                                                               275729, -- admitad_promo
                                                               90289, --gnatovsky
                                                               1355412, --webhostadvice
                                                               1118196, --ankurkhanna
                                                               607243, --megabonuscom
                                                               701906 --aliexpressradar
                                  ), 0,
                                  final_payment_usd + final_payment_to_system_usd < toUInt32(60000) AND
                                  last_status = 'pending'
                                      AND webmaster_id NOT IN (393445, -- adm_green
                                                               1028843, -- ad_ua_green
                                                               683873, -- 00advertiser00
                                                               275729, -- admitad_promo
                                                               90289, --gnatovsky
                                                               1355412, --webhostadvice
                                                               1118196, --ankurkhanna
                                                               607243, --megabonuscom
                                                               701906 --aliexpressradar
                                      ), final_payment_to_system_usd * percent,
                                  last_status = 'pending' AND webmaster_id IN (90289, --gnatovsky
                                                                               1355412, --webhostadvice
                                                                               1118196, --ankurkhanna
                                                                               607243, --megabonuscom
                                                                               701906 --aliexpressradar
                                      ), final_payment_to_system_usd * percent,
                                  0
                              )                                                                           AS forecast_system_usd,
                          if(last_status = 'approved' AND webmaster_id IN (1018138, -- ad_system
                                                                           205609, -- admitad_deeplink
                                                                           694705 -- admitad_india
                              ), final_payment_usd,
                             0)                                                                           AS web_revenue_approved,
                          if(last_status = 'pending' AND webmaster_id IN (1018138, -- ad_system
                                                                          205609, -- admitad_deeplink
                                                                          694705 -- admitad_india
                              ), (final_payment_usd ) * percent,
                             0)                                                                           AS web_revenue_forecast


                   FROM report.aggregated_revenue
                   WHERE toDate(report_datetime) = toDate('$report_day')
                          AND month = '$accounting_month'
                     AND program_id NOT IN (1182,
                                            17789,
                                            21659
                       )
                     AND webmaster_id != 598980
                      )
                  GROUP BY webmaster_id, source_currency, country_of_action, program_id, percent, month, team_of_action,
                           team_of_program
                  )
                  ANY LEFT JOIN
              (
                  SELECT program_id                                                              AS offer_id,
                         toFloat64(sumIf(final_payment_usd,
                                         last_status = 'approved' AND webmaster_id NOT IN (393445, -- adm_green
                                                                                           1028843, -- ad_ua_green
                                                                                           683873, -- 00advertiser00
                                                                                           275729 -- admitad_promo
                                             )))                                                 AS apr_web_ali_usd,
                         toFloat64(sumIf(final_payment_usd, last_status = 'approved' AND webmaster_id IN (1018138, -- ad_system
                                                                                                         205609, -- admitad_deeplink
                                                                                                         694705 -- admitad_india
                                                                                                         )))                           AS web_month_ali,
                         toFloat64(sumIf(final_payment_to_system_usd, last_status = 'approved')) AS apr_to_system_ali
                  FROM report.aggregated_revenue
                  WHERE toDate(report_datetime) = toDate('$report_day')
                    AND month = '$accounting_month'
                    AND webmaster_id != 598980 -- dtraffic
                    AND program_id IN (22586, 6115)
                  GROUP BY program_id
                  )
              USING offer_id
WHERE team_sub_account != 'OD-Affiliate Vendor Network'
         ORDER BY offer_id, web_id

    )
    ANY LEFT JOIN
(
    SELECT id                                                                  AS offer_id,
           argMax(user__username, snapshot_datetime)                           AS offer_login,
           dictGetString('default.categories', 'name',
                                    toUInt64(arrayJoin(if(
                                                arrayDistinct(argMax(child_categories_internal, snapshot_datetime)) =
                                                [],
                                                [0],
                                                arrayDistinct(argMax(child_categories_internal, snapshot_datetime)))))) AS  offer_category
    FROM default.affiliate_program
    GROUP BY offer_id
    )
USING offer_id